package cz.maca.sampleapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Like(
    val count: Int?
) : Parcelable