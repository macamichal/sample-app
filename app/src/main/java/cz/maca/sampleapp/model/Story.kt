package cz.maca.sampleapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Story(
    val id: String?,
    @SerializedName("short_id") val shortId: String?,
    val revision: Int?,
    val user: User?,
    val version: Int?,
    @SerializedName("cover_src") val coverSrc: String?,
    @SerializedName("cover_bg") val coverBg: String?,
    val title: String?,
    val likes: Like?,
    val private: Boolean?
) : Parcelable