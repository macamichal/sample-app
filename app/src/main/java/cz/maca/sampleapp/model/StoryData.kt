package cz.maca.sampleapp.model

data class StoryData(
    val data: List<Story>?
)