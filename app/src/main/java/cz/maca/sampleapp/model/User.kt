package cz.maca.sampleapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: String?,
    val revision: Int?,
    @SerializedName("display_name") val displayName: String?,
    @SerializedName("avatar_image_url") val avatarImageUrl: String?,
    @SerializedName("avatar_image_bg") val avatarImageBg: String?,
    val followers: Int?,
    val following: Int?,
    @SerializedName("explicitly_followed")val explicitlyFollowed: Boolean?,
    @SerializedName("implicitly_followed")val implicitlyFollowed: Boolean?,
    val blocked: Boolean?,
    val stories: Int?,
    @SerializedName("follow_request_sent")val followRequestSent: Boolean?,
    @SerializedName("follow_request_received")val followRequestReceived: Boolean?,
    val username: String?,
    val private: Boolean?
) : Parcelable