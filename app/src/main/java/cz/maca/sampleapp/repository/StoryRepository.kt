package cz.maca.sampleapp.repository

import cz.maca.sampleapp.model.Story
import cz.maca.sampleapp.model.StoryData
import cz.maca.sampleapp.repository.retrofit.Retrofit
import cz.maca.sampleapp.repository.retrofit.api.StoryApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import cz.maca.sampleapp.util.Result
import retrofit2.Response
import java.io.IOException
import java.lang.Exception

class StoryRepository {

    private val storyApi: StoryApi = Retrofit().creteClient(StoryApi::class.java, "https://api.steller.co")

    suspend fun loadData(): Result<MutableList<Story>> {
        return withContext(Dispatchers.IO) {
            val response: Response<StoryData>

            try {
                response = storyApi.getStories().execute()
            } catch (exception: IOException) {
                return@withContext Result.Error(exception)
            }

            return@withContext if (response.isSuccessful) {
                Result.Success(response.body().data!!.toMutableList())
            } else {
                Result.Error(Exception("${response.code()}: ${response.message()}"))
            }
        }
    }

}