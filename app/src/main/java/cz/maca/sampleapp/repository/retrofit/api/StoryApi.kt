package cz.maca.sampleapp.repository.retrofit.api

import cz.maca.sampleapp.model.StoryData
import retrofit2.Call
import retrofit2.http.GET

interface StoryApi {

    @GET("/v1/users/76794126980351029/stories")
    fun getStories(): Call<StoryData>
}