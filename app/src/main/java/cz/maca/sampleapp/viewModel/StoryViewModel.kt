package cz.maca.sampleapp.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.maca.sampleapp.model.Story
import cz.maca.sampleapp.repository.StoryRepository
import cz.maca.sampleapp.util.Result
import kotlinx.coroutines.launch

class StoryViewModel : ViewModel() {

    private var TAG = StoryViewModel::class.java.simpleName

    private val storyRepository = StoryRepository()

    private val storyData = MutableLiveData<Result<MutableList<Story>>>()
    val storyState: LiveData<Result<MutableList<Story>>> = storyData

    fun loadData() {
        Log.d(TAG, "load data")
        storyData.value = Result.Loading

        viewModelScope.launch {
            storyData.value = storyRepository.loadData()
        }
    }

}