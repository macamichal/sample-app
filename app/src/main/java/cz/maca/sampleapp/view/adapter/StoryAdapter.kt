package cz.maca.sampleapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.maca.sampleapp.R
import cz.maca.sampleapp.model.Story
import kotlinx.android.synthetic.main.item_list_story.view.*

class StoryAdapter(var dataSet: MutableList<Story>, val onClick: (Story) -> Unit) :
    RecyclerView.Adapter<StoryAdapter.StoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryViewHolder = StoryViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_list_story, parent, false)
    )

    override fun onBindViewHolder(holder: StoryViewHolder, position: Int) {
        holder.bind(dataSet[position], onClick)
    }

    override fun getItemCount(): Int = dataSet.size

    class StoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title = view.text_title
        private val image = view.img_cover
        private val like = view.text_like
        private val card = view.card_item

        fun bind(story: Story, onClick: (Story) -> Unit) {
            title.text = story.title
            like.text = story.likes?.count.toString()
            Glide.with(image.context).load(story.coverSrc).into(image)
            card.setOnClickListener { onClick(story) }
        }
    }
}