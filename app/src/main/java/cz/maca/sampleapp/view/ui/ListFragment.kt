package cz.maca.sampleapp.view.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import cz.maca.sampleapp.Constants
import cz.maca.sampleapp.R
import cz.maca.sampleapp.model.Story
import cz.maca.sampleapp.view.adapter.StoryAdapter
import cz.maca.sampleapp.viewModel.StoryViewModel
import kotlinx.android.synthetic.main.fragment_list.*
import cz.maca.sampleapp.util.Result
import cz.maca.sampleapp.view.show

class ListFragment : Fragment() {

    private var TAG = ListFragment::class.java.simpleName

    private val storyAdapter = StoryAdapter(arrayListOf()) { openDetail(it) }
    private lateinit var storyViewModel: StoryViewModel
    private lateinit var stories: List<Story>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        storyViewModel = ViewModelProviders.of(this)[StoryViewModel::class.java]


        list_story.apply {
            adapter = storyAdapter
        }

        subscribeUi()

        if (savedInstanceState == null) {
            Log.d(TAG, "load data")
            storyViewModel.loadData()
        }

    }

    private fun subscribeUi() {
        storyViewModel.storyState.observe(this, Observer<Result<MutableList<Story>>> { result ->

            Log.d(TAG, "result: $result")

            when (result) {
                Result.Loading -> {
                    showProgress(true)
                }
                is Result.Success<MutableList<Story>> -> {
                    showProgress(false)
                    Log.d(TAG, "data size: ${result.data.size}")
                    storyAdapter.dataSet = result.data
                    stories = result.data
                }
                is Result.Error -> {
                    showProgress(false)
                    Log.d(TAG, "error: ${result.exception}")
                    Toast.makeText(context, result.exception.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun showProgress(value: Boolean) {
        progress_story.show(value)
        list_story.show(!value)
    }

    private fun openDetail(item: Story) {
        Log.d(TAG, "detail: $item")
        val bundle = Bundle()
        bundle.putParcelableArrayList(Constants().BUNDLE_DETAIL, ArrayList(stories))
        (activity as MainActivity).replaceFragment(DetailFragment(), bundle = bundle)
    }
}