package cz.maca.sampleapp.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import cz.maca.sampleapp.Constants
import cz.maca.sampleapp.R
import cz.maca.sampleapp.model.Story
import cz.maca.sampleapp.view.adapter.StoryPagerAdapter

class DetailFragment : Fragment() {

    private lateinit var stories: List<Story>
    private lateinit var viewPager: ViewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)

        viewPager = view.findViewById(R.id.viewPager)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        stories = arguments?.getParcelableArrayList<Story>(Constants().BUNDLE_DETAIL)?.toList() ?: listOf()


        viewPager.adapter =  fragmentManager?.let { StoryPagerAdapter(it, stories) }
    }

}