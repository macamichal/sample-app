package cz.maca.sampleapp.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import cz.maca.sampleapp.Constants
import cz.maca.sampleapp.R
import cz.maca.sampleapp.model.Story
import kotlinx.android.synthetic.main.item_list_story.*

class StoryFragment : Fragment() {

    private lateinit var story: Story

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_story, container, false)
        story = arguments?.getParcelable(Constants().BUNDLE_ONE_DETAIL)!!

        return view
    }

    companion object {
        fun newInstance(story: Story): StoryFragment {
            val args = Bundle()
            args.putParcelable(Constants().BUNDLE_ONE_DETAIL, story)
            val fragment = StoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        text_title.text = story.title
        text_like.text = story.likes?.count.toString()
        Glide.with(img_cover.context).load(story.coverSrc).into(img_cover)
    }
}