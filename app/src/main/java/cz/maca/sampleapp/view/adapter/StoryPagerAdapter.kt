package cz.maca.sampleapp.view.adapter

import cz.maca.sampleapp.model.Story
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import cz.maca.sampleapp.view.ui.StoryFragment

class StoryPagerAdapter(fragmentManager: FragmentManager, private val stories: List<Story>) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return StoryFragment.newInstance(stories[position])
    }

    override fun getCount(): Int {
        return stories.size
    }

}